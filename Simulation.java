import java.io.*;
import java.math.*;
import java.util.*;
import java.text.DecimalFormat;

/** 
 * @author Erin Wild eewild@gmail.com
 * @url https://bitbucket.org/erinwild/competitive-helping-model/
 **/

public class Simulation {
	public static final int timeSteps = 5000; // number of times to do one iteration of the game
	public static final int popSize = 100000; // population size
	public static final int numRuns = 5000; // number of times to run the same simulation (with the same starting parameters)
	public static final double mutationRate = 0.1;
	
	public static MersenneTwisterFast rng;
	
	public static final boolean homogenous = true;
	public static int[] sortingIndex;
	
	public static File fileRunDat;
	public static PrintWriter printerRunDat;
	
	public static DecimalFormat twoDecimal = new DecimalFormat("###.##");
	public static DecimalFormat threeDecimal = new DecimalFormat("#.###");
	
	public static void main(String[] args) throws FileNotFoundException {
		
		//long startTime = System.nanoTime();
		
		//String title;
		double[] zSet = {2, 5, 10};
		for(double z : zSet) {
			//for(int Sm = 5; Sm <= popSize/2; Sm = Sm + 10000) {
			for(double k = 0.605; k <= 0.7000005; k += 0.005) {
				
				rng = new MersenneTwisterFast();
				
				PopulationGroups groupies;
				//title = "groupies";
				int numGroups = 2;
				double mBase = 1.0;
				double xBase = 0.5;
				double zBase = z;
				double kBase = k;
				int[] groupPopSizes = {popSize/numGroups,popSize/numGroups};
				double[] groupHelp = new double[numGroups];
				double[] groupM = {mBase,mBase};
				double[] groupX = {xBase,xBase};
				double[] groupZ = {zBase,zBase};
				double[] groupK = {kBase,kBase};
				double mutSize = 0.001; // maximum amount that a mutation can alter +/- current help
				
				fileRunDat = new File("z" + twoDecimal.format(zBase) + "-k" + threeDecimal.format(kBase) + ".txt");
				printerRunDat = new PrintWriter(fileRunDat);
				
				for(int sim = 1; sim <= numRuns; sim++) {
					//fileRunDat = new File("z-" + twoDecimal.format(zBase) + "-Sm-0" + Sm/10 + "-Run" + sim + ".txt");
					//printerRunDat = new PrintWriter(fileRunDat);
					/* Initializing population */
					for(int i = 0; i < numGroups; i++) {
						groupHelp[i] = rng.nextDouble()*mBase;
					}
					groupies = new PopulationGroups(popSize,numGroups);
					groupies.initializeAttributes(groupPopSizes,groupHelp,groupM,groupX,groupZ,groupK);
					/* Time step */
					for(int time = 0; time < timeSteps; time++) {
						long startTime = System.nanoTime();
						groupies.calculateHelpReceived();
						groupies.calculatePayoffs();
						//printerRunDat.print(groupies.groups[0].getGroupSize() + " ");
						//for(int i = 0; i < numGroups; i++) {
						//	printerRunDat.print(groupies.groups[i].getHelp() + " ");
						//}
						//for(int i = 0; i < numGroups; i++) {
						//	printerRunDat.print(groupies.groups[i].getPayoff() + " ");
						//}
						//printerRunDat.println("");
						double[] currentHelp = new double[numGroups];
						double[] currentPayoff = new double[numGroups];
						double[] mutAmount = new double[numGroups];
						double[] newHelp = new double[numGroups];
						for(int i = 0; i < numGroups; i++) {
							currentHelp[i] = groupies.groups[i].getHelp();
							currentPayoff[i] = groupies.groups[i].getPayoff();
							mutAmount[i] = rng.nextDouble()*mutSize;
							newHelp[i] = groupies.groups[i].getHelp();
						}
						
						for(int i = 0; i < numGroups; i++) {
							groupies.groups[(i+numGroups-1)%numGroups].setHelp(currentHelp[(i+numGroups-1)%numGroups]);
							if(rng.nextBoolean()) {
								mutAmount[i] = mutAmount[i]*(-1); // subtract mutation amount
							}
							if(currentHelp[i]+mutAmount[i] >= mBase) {
								groupies.groups[i].setHelp(mBase);
							} else if(currentHelp[i]+mutAmount[i] <= 0) {
								groupies.groups[i].setHelp(0);
							} else {
								groupies.groups[i].setHelp(currentHelp[i]+mutAmount[i]);
							}
							groupies.calculateHelpReceived();
							groupies.groups[i].calculatePayoff();
							if(groupies.groups[i].getPayoff() >= currentPayoff[i]) {
								newHelp[i] = groupies.groups[i].getHelp();
							}
						}
						
						for(int i = 0; i < numGroups; i++) {
							groupies.groups[i].setHelp(newHelp[i]);
						}
						
						/*
						 // proportion mutation
						 if(rng.nextDouble() <= Math.abs(groupies.groups[0].getPayoff() - groupies.groups[1].getPayoff())) {
							if(groupies.groups[0].getPayoff() > groupies.groups[1].getPayoff() && groupies.groups[1].getGroupSize() > 0) {
						 groupies.groups[0].setGroupSize(groupies.groups[0].getGroupSize() + 10000);
						 groupies.groups[1].setGroupSize(groupies.groups[1].getGroupSize() - 10000);
							} else if(groupies.groups[0].getPayoff() < groupies.groups[1].getPayoff() && groupies.groups[0].getGroupSize() > 0) {
						 groupies.groups[0].setGroupSize(groupies.groups[0].getGroupSize() - 10000);
						 groupies.groups[1].setGroupSize(groupies.groups[1].getGroupSize() + 10000);
							}
						 }
						 */
						
						//long endTime = System.nanoTime();
						//System.out.println("time step took " + (endTime - startTime) + " ns");
					}
					groupies.calculateHelpReceived();
					groupies.calculatePayoffs();
					for(int i = 0; i < numGroups; i++) {
						printerRunDat.print(groupies.groups[i].getHelp() + " ");
					}
					for(int i = 0; i < numGroups; i++) {
						printerRunDat.print(groupies.groups[i].getPayoff() + " ");
					}
					printerRunDat.println("");
					//printerRunDat.close();
					
				}
				printerRunDat.close();
			}
		}
	
	
	//		sortingIndex = new int[popSize]; // initializing sorting index for tournaments
	//		for(int i = 0; i < popSize; i++) {
	//			sortingIndex[i] = i;
	//		}
	//
	//		if(homogenous) {
	//			/* Homogeneous Population: examining the effect of parameters on h* */
	//			Population homoPop;
	//
	//			double hBase = 1; // default value for h
	//			double mBase = 10; // default value for m
	//			double xBase = 0.5; // default value for x
	//			double zBase = 1; // default value for z
	//			double kBase = 0; // default value for k
	//
	//			/* Varying m */
	//			title = "varyingM";
	//			double[] mSet = {1,5,20};
	//			for(double m : mSet) {
	//				for(int sim = 1; sim <= numRuns; sim++) {
	//					initializePrinters(title,m,xBase,zBase,kBase,sim);
	//					/* Initializing population */
	//					homoPop = new Population(popSize);
	//					homoPop.initializeAttributes(hBase,m,xBase,zBase,kBase);
	//					/* Time step */
	//					for(int i = 0; i < timeSteps; i++) {
	//						homoPop.calculateHelpReceived();
	//						homoPop.calculatePayoffs();
	//						printData(homoPop);
	//						tournament(homoPop,7);
	//						mutation(homoPop,mutationRate,m);
	//					}
	//					printerRunDat.close();
	//				}
	//			}
	//
	//			/* Varying x */
	//			title = "varyingX";
	//			for(double x = 0.1; x <= 0.9; x += 0.1) {
	//				for(int sim = 1; sim <= numRuns; sim++) {
	//					initializePrinters(title,mBase,x,zBase,kBase,sim);
	//					/* Initializing population */
	//					homoPop = new Population(popSize);
	//					homoPop.initializeAttributes(hBase,mBase,x,zBase,kBase);
	//					/* Time step */
	//					for(int i = 0; i < timeSteps; i++) {
	//						homoPop.calculateHelpReceived();
	//						homoPop.calculatePayoffs();
	//						printData(homoPop);
	//						tournament(homoPop,7);
	//						mutation(homoPop,mutationRate,mBase);
	//					}
	//					printerRunDat.close();
	//				}
	//			}
	//
	//			/* Varying z */
	//			title = "varyingZ";
	//			double[] zSet = {0,0.25,0.5,0.75,1.25,1.5,1.75,2,2.25,2.5,2.75,3};
	//			for(double z : zSet) {
	//				for(int sim = 1; sim <= numRuns; sim++) {
	//					initializePrinters(title,mBase,xBase,z,kBase,sim);
	//					/* Initializing population */
	//					homoPop = new Population(popSize);
	//					homoPop.initializeAttributes(hBase,mBase,xBase,z,kBase);
	//					/* Time step */
	//					for(int i = 0; i < timeSteps; i++) {
	//						homoPop.calculateHelpReceived();
	//						homoPop.calculatePayoffs();
	//						printData(homoPop);
	//						tournament(homoPop,7);
	//						mutation(homoPop,mutationRate,mBase);
	//					}
	//					printerRunDat.close();
	//				}
	//			}
	//
	//			/* Varying k */
	//			title = "varyingK";
	//			for(double k = 0.5; k <= 2; k += 0.5) {
	//				for(int sim = 1; sim <= numRuns; sim++) {
	//					initializePrinters(title,mBase,xBase,zBase,k,sim);
	//					/* Initializing population */
	//					homoPop = new Population(popSize);
	//					homoPop.initializeAttributes(hBase,mBase,xBase,zBase,k);
	//					/* Time step */
	//					for(int i = 0; i < timeSteps; i++) {
	//						homoPop.calculateHelpReceived();
	//						homoPop.calculatePayoffs();
	//						printData(homoPop);
	//						tournament(homoPop,7);
	//						mutation(homoPop,mutationRate,mBase);
	//					}
	//					printerRunDat.close();
	//				}
	//			}
	//
	////			/* In-depth z analysis: random z in [0,3] */
	////			for(int sim = 0; sim < 500; sim++) {
	////				double z = rng.nextDouble()*3;
	////				initializePrinters(title,sim);
	////				/* Initializing population */
	////				homoPop = new Population(popSize);
	////				homoPop.initializeAttributes(hBase,mBase,xBase,z,kBase);
	////				/* Time step */
	////				for(int i = 0; i < timeSteps; i++) {
	////					homoPop.calculateHelpReceived();
	////					homoPop.calculatePayoffs();
	////					printData(homoPop);
	////					tournament(homoPop,10);
	////					mutation(homoPop,mutationRate,mBase);
	////				}
	////				printerRunDat.close();
	////			}
	//
	//		} else {
	//			/* Heterogeneous Population */
	//			title = "heterogeneous";
	//			Population heteroPop;
	//
	//			double[] hIndividuals = new double[popSize];
	//			double[] mIndividuals = new double[popSize];
	//			double[] xIndividuals = new double[popSize];
	//			double[] zIndividuals = new double[popSize];
	//			double[] kIndividuals = new double[popSize];
	//
	//			double hMax = 10;
	//			double mMax = 10;
	//			double zMax = 5;
	//			double kMax = 2;
	//
	//			for(int i = 0; i < popSize; i++) {
	//				hIndividuals[i] = rng.nextDouble() * hMax; // h in [0,100]
	//				mIndividuals[i] = rng.nextDouble() * mMax; // m in [0,100]
	//				xIndividuals[i] = rng.nextDouble();		// x in [0,1]
	//				zIndividuals[i] = 1; //!rng.nextDouble() * zMax; // z in [0,5]
	//				kIndividuals[i] = rng.nextDouble() * kMax; // k in [0,2]
	//			}
	//			for(int sim = 1; sim <= numRuns; sim++) {
	//				initializePrinters(title,sim);
	//				/* Initializing population */
	//				heteroPop = new Population(popSize);
	//				heteroPop.initializeAttributes(hIndividuals,mIndividuals,xIndividuals,zIndividuals,kIndividuals);
	//				/* Time step */
	//				for(int i = 0; i < timeSteps; i++) {
	//					heteroPop.calculateHelpReceived();
	//					heteroPop.calculatePayoffs();
	//					printData(heteroPop);
	//					tournament(heteroPop,10);
	//					variationOps(heteroPop,mutationRate,hMax,mMax,zMax,kMax);
	//				}
	//				printerRunDat.close();
	//			}
	//		}
	
	//long endTime = System.nanoTime();
	//System.out.println(title + " took " + (endTime - startTime) + " ns");
	
}

public static void initializePrinters(String title, double m, double x, double z, double k, int run) throws FileNotFoundException {
	// initializes Files/PrintWriters for homogeneous populations
	fileRunDat = new File(title + "-N" + popSize + "t" + timeSteps + "m" + twoDecimal.format(m) + "x" + twoDecimal.format(x) + "z" + twoDecimal.format(z) + "k" + twoDecimal.format(k) + "-Run" + run + ".txt");
	printerRunDat = new PrintWriter(fileRunDat);
}

public static void initializePrinters(String title, int run) throws FileNotFoundException {
	// initializes Files/PrintWriters for heterogeneous populations
	fileRunDat = new File(title + "-N" + popSize + "t" + timeSteps + "-Run" + run + ".txt");
	printerRunDat = new PrintWriter(fileRunDat);
}

public static void printData(Population populace) throws FileNotFoundException {
	// prints various information about the Population to a .txt file

	// calculates mean payoff and mean help produced
	// finds best payoff and corresponding help produced
	double avgPayoff = 0;
	double avgHelp = 0;
	double bestPayoff = -100000;
	double bestHelp = -1;
	double bestM = -1;
	double bestX = -1;
	double bestZ = -1;
	double bestK = -1;
	for(int j = 0; j < popSize; j++) {
		avgPayoff += populace.individuals[j].getPayoff();
		avgHelp += populace.individuals[j].getHelp();
		if(populace.individuals[j].getPayoff() > bestPayoff) {
			bestPayoff = populace.individuals[j].getPayoff();
			bestHelp = populace.individuals[j].getHelp();
			bestM = populace.individuals[j].getMaxFitnessBenefits();
			bestX = populace.individuals[j].getBenefitsCurve();
			bestZ = populace.individuals[j].getPartnerChoice();
			bestK = populace.individuals[j].getPassiveHelp();
		}
	}
	avgPayoff = avgPayoff/popSize;
	avgHelp = avgHelp/popSize;

	// calculates standard deviation of mean payoff and mean help produced
	double stdPayoff = 0;
	double stdHelp = 0;
	for(int j = 0; j < popSize; j++) {
		stdPayoff += Math.pow(populace.individuals[j].getPayoff()-avgPayoff,2);
		stdHelp += Math.pow(populace.individuals[j].getHelp()-avgHelp,2);
	}
	stdPayoff = Math.sqrt(stdPayoff/popSize);
	stdHelp = Math.sqrt(stdHelp/popSize);

	// calculates 95% confidence interval of mean payoff and mean help produced
	double ciPayoff = 1.96*(stdPayoff/Math.sqrt(popSize));
	double ciHelp = 1.96*(stdHelp/Math.sqrt(popSize));

	// prints data
	if(homogenous) {
		printerRunDat.println(avgPayoff + " " + stdPayoff + " " + ciPayoff + " " + bestPayoff + " " + avgHelp + " " + stdHelp + " " + ciHelp + " " + bestHelp);
	} else {
		printerRunDat.println(avgPayoff + " " + stdPayoff + " " + ciPayoff + " " + bestPayoff + " " + avgHelp + " " + stdHelp + " " + ciHelp + " " + bestHelp +  " " + bestM +  " " + bestX +  " " + bestZ +  " " + bestK);
	}
}

public static void tournament(Population populace, int size) {
	// size tournament selection: chooses n=size individuals at random; clones best two and discards worst two
	if(size < 4 || size > populace.getPopulationSize()) {
		throw new IllegalArgumentException("Tournament size must be between 4 and the population size.");
	} else {
		int t;
		int swap;
		for(int i = 0; i < size; i++) {
			t = rng.nextInt(popSize); // select random tournament members
			swap = sortingIndex[i];
			sortingIndex[i] = sortingIndex[t];
			sortingIndex[t] = swap; // swap tournament members to front of sorting array
		}

		// sort the tournament into lowest fitness first
		do {
			t = 0;
			for(int i = 0; i < size - 1; i++) {
				if(populace.individuals[sortingIndex[i]].getPayoff() > populace.individuals[sortingIndex[i+1]].getPayoff()) {
					// if out of order, swap
					swap = sortingIndex[i];
					sortingIndex[i] = sortingIndex[i+1];
					sortingIndex[i+1] = swap;
					t = 1;
				}
			}
			} while(t == 1);

			// worst 2 individuals are replaced by clones of best 2 individuals
			Population.clone(populace.individuals[sortingIndex[size-1]],populace.individuals[sortingIndex[0]]);
			Population.clone(populace.individuals[sortingIndex[size-2]],populace.individuals[sortingIndex[1]]);
		}
	}

public static void mutation(Population populace, double rate, double maxHelp) {
	// randomly mutates the level of active help of Individuals given mutation rate
	if(rate < 0 || rate > 1) {
		throw new IllegalArgumentException("Mutation rate must be between 0 and 1.");
	} else {
		Population.mutation(populace.individuals[sortingIndex[0]],mutationRate,maxHelp);
		Population.mutation(populace.individuals[sortingIndex[1]],mutationRate,maxHelp);
	}
}

public static void variationOps(Population populace, double rate, double maxHelp, double maxM, double maxZ, double maxK) {
	// performs crossover and randomly mutates each of the parameters of Individuals given mutation rate
	if(rate < 0 || rate > 1) {
		throw new IllegalArgumentException("Mutation rate must be between 0 and 1.");
	} else {
		Population.crossover(populace.individuals[sortingIndex[0]],populace.individuals[sortingIndex[1]]);
		Population.mutation(populace.individuals[sortingIndex[0]],mutationRate,maxHelp,maxM,maxZ,maxK);
		Population.mutation(populace.individuals[sortingIndex[1]],mutationRate,maxHelp,maxM,maxZ,maxK);
	}
}
}