import matplotlib.pyplot as plt
import numpy as np
import math

fig, ax = plt.subplots(ncols=1)
ax.set(adjustable='box-forced', aspect='equal')

z_set = [10, 5, 2, 1.5, 1, 0.5, 0]
x = np.arange(0, 1.01, 0.01)
N = 100000
m = 1
k = 0



#light_blue = '#06AED5'
#dark_blue = '#086788'
#yellow = '#F0C808'
#bone = '#FFF1D0'
#red = '#DD1C1A'
#colours = [dark_blue,yellow,light_blue,red,yellow,dark_blue,light_blue]
colours = [plt.cm.viridis(i) for i in np.linspace(0, 1, 8)]

ls = ['-','--','-','--','-','--','-']

c_index = 0
for z in z_set:
	z_label = 'z = %r' % (z_set[c_index])
	plt.plot(x, (-x+np.sqrt(x*(1/2.0)*(N-2)*((z+1)*N-1))/float(N-1)), linestyle=ls[c_index], linewidth=1.5, color=colours[c_index], label=z_label)
	c_index += 1

plt.ylabel('h*')
plt.xlabel('x')
plt.ylim(0, 1)
plt.xlim(0, 1)
plt.legend()
#plt.tight_layout()
fig.savefig('help_z_vs_x.eps', bbox_inches='tight')
