import glob
import numpy as np
import matplotlib.pyplot as plt

fig = plt.figure()#figsize=(12,8))

time = 4999

light_blue = '#06AED5'
dark_blue = '#086788'
yellow = '#F0C808'
bone = '#FFF1D0'
red = '#DD1C1A'

ax_index = 0
for z in [0, 0.5, 1, 2, 5, 10]:
	path_name = ('/Volumes/Optical Bay HD/Dropbox/School/Doctoral Research/Competitive Helping Model/Simulations/Sm-Sp splits - varying z/z-%r-Sm-05-Run*.txt') % z
	data = []
	for filename in glob.iglob(path_name):
		file_data = open(filename, 'r')
		lines = np.loadtxt(file_data, usecols=(0, 1), unpack=True)
		file_data.close()
		
		ax = fig.add_subplot(231+ax_index)
		if lines[0][-1] == 0:
			colour = dark_blue
		elif lines[1][-1] == 0:
			colour = yellow
		else:
			colour = red
		plt.plot(lines[0], lines[1], c=colour)
		plt.plot(lines[0][-1], lines[1][-1], 'kD')

	if z == 0.5 or z == 1 or z == 5 or z == 10:
		ax.set_yticklabels([])
	else:
		plt.ylabel('h$_m$')
	if z >= 2:
		plt.xlabel('h$_p$')
	else:
		ax.set_xticklabels([])

	if z==0:
		ax.text(0.97, 0.97, 'z=0', #i',
				horizontalalignment='right',
				verticalalignment='top',
				fontsize='x-large',
				fontweight='bold',
				transform=ax.transAxes)
	elif z==0.5:
		ax.text(0.97, 0.97, 'z=0.5', #ii',
				horizontalalignment='right',
				verticalalignment='top',
				fontsize='x-large',
				fontweight='bold',
				transform=ax.transAxes)
	elif z==1:
		ax.text(0.97, 0.97, 'z=1', #iii',
				horizontalalignment='right',
				verticalalignment='top',
				fontsize='x-large',
				fontweight='bold',
				transform=ax.transAxes)
	elif z==2:
		ax.text(0.97, 0.97, 'z=2', #iv',
				horizontalalignment='right',
				verticalalignment='top',
				fontsize='x-large',
				fontweight='bold',
				transform=ax.transAxes)
	elif z==5:
		ax.text(0.97, 0.97, 'z=5', #v',
				horizontalalignment='right',
				verticalalignment='top',
				fontsize='x-large',
				fontweight='bold',
				transform=ax.transAxes)
	elif z==10:
		ax.text(0.97, 0.97, 'z=10', #vi',
				horizontalalignment='right',
				verticalalignment='top',
				fontsize='x-large',
				fontweight='bold',
				transform=ax.transAxes)

	plt.axis([-0.02, 1.02, -0.02, 1.02])
	ax_index += 1

plt.tight_layout()
plt.show()