import glob
import numpy as np
import matplotlib.pyplot as plt

time = 4999

z_values = ['0', '0.5', '1', '1.5', '2', '3', '5', '10']

for i in z_values:
	fig, axes = plt.subplots(figsize=(8,4), ncols=2, sharex=True, sharey=True)
	
	path_name_00 = '/Volumes/Optical Bay HD/Dropbox/School/Doctoral Research/Competitive Helping Model/Simulations/Sm-Sp splits - varying z/z-%s-Sm-01-Run*.txt' % i
	path_name_05 = '/Volumes/Optical Bay HD/Dropbox/School/Doctoral Research/Competitive Helping Model/Simulations/Sm-Sp splits - varying z/z-%s-Sm-05-Run*.txt' % i
	#path_name_00 = '/Users/Erin/Dropbox/School/Doctoral Research/Competitive Helping Model/Simulations/Sm-Sp splits - varying z/z-0-Sm-00-Run*.txt'
	#path_name_05 = '/Users/Erin/Dropbox/School/Doctoral Research/Competitive Helping Model/Simulations/Sm-Sp splits - varying z/z-0-Sm-05-Run*.txt'


	for filename in glob.iglob(path_name_00):
		lines = np.loadtxt(filename, usecols=(0, 1), unpack=True)
		axes[0].plot(lines[0], lines[1], c='0.45')
		axes[0].plot(lines[0][-1], lines[1][-1], 'kD')

	axes[0].set_ylabel('h$_m$')
	axes[0].text(0.97, 0.97, 'i',
		horizontalalignment='right',
		verticalalignment='top',
		fontsize='x-large',
		fontweight='bold',
		transform=axes[0].transAxes)

	for filename in glob.iglob(path_name_05):
		lines = np.loadtxt(filename, usecols=(0, 1), unpack=True)
		axes[1].plot(lines[0], lines[1], c='0.45')
		axes[1].plot(lines[0][-1], lines[1][-1], 'kD')


	axes[1].text(0.97, 0.97, 'ii',
		horizontalalignment='right',
		verticalalignment='top',
		fontsize='x-large',
		fontweight='bold',
		transform=axes[1].transAxes)
	axes[1].yaxis.tick_right()
	axes[1].set_ylabel('h$_m$')
	axes[1].yaxis.set_label_position('right')
		
	for ax in axes:
		plt.axis([-0.02, 1.02, -0.02, 1.02])
		ax.set(adjustable='box-forced', aspect='equal')
		ax.set_xlabel('h$_p$')

	plt.tight_layout()
	fig.savefig('eqtraj_z%s_01v05.eps' % i, bbox_inches='tight')
	#plt.show()