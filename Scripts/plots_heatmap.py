import matplotlib.pyplot as plt
import numpy as np
import glob
import math

def extract_last_line(txt_file):
	last_lines = []
	for filename in sorted(glob.iglob(txt_file)):
		file_data = open(filename, 'r')
		lines = file_data.readlines()
		file_data.close()
		last = lines[-1].split()
		for i in range(0,len(last)):
			last[i] = float(last[i])
		last_lines.append(last)
	last_lines = np.array(last_lines)
	return last_lines

path_prefix = '/Volumes/Optical Bay HD/Dropbox/School/Doctoral Research/Competitive Helping Model/Simulations'

data1 = extract_last_line(path_prefix + '/Sm-Sp splits - varying z/z-2-*Run16.txt')
data2 = extract_last_line(path_prefix + '/Sm-Sp splits - varying z/z-2-*Run11.txt')
data = np.concatenate((data1[0:8],data2[8:11]), axis=0)

rows = np.arange(0,1.1,0.1)

fig, ax = plt.subplots(figsize=(4,6))
heatmap = ax.pcolor(data[:,0:2], cmap='viridis_r')
heatmap.set_clim(vmin=0, vmax=0.45)
cbar = plt.colorbar(heatmap, pad = 0.45)
cbar.set_label('help')
ax.set_xticks([0.5,1.5])
ax.set_xticklabels(['h$_m$','h$_p$'])
ax.set_yticks(np.arange(0,11)+0.5)
ax.set_yticklabels(rows)
ax.set_ylabel('S$_m$')
ax.set_ylim(0,11)
ax.set_xlim(0,2)
ax.set_title('Help',size='medium')

ax2 = ax.twinx()
ax2.set_ylabel('S$_p$')
ax2.set_ylim(0,11)
ax2.set_xlim(0,2)
ax2.set_yticks(np.arange(0,11)+0.5)
ax2.set_yticklabels(rows[::-1])
ax.set_aspect('equal')
ax2.set_aspect('equal')

fig2, ax3 = plt.subplots(figsize=(4,6))
heatmap2 = ax3.pcolor(data[:,2:4], cmap='plasma_r')
heatmap2.set_clim(vmin=0, vmax=0.28)
cbar2 = plt.colorbar(heatmap2, pad = 0.45)
cbar2.set_label('payoff')
ax3.set_xticks([0.5,1.5])
ax3.set_xticklabels(['W$_m$','W$_p$'])
ax3.set_yticks(np.arange(0,11)+0.5)
ax3.set_yticklabels(rows)
ax3.set_ylabel('S$_m$')
ax3.set_ylim(0,11)
ax3.set_xlim(0,2)
ax3.set_title('Payoff',size='medium')

ax4 = ax3.twinx()
ax4.set_ylabel('S$_p$')
ax4.set_ylim(0,11)
ax4.set_xlim(0,2)
ax4.set_yticks(np.arange(0,11)+0.5)
ax4.set_yticklabels(rows[::-1])
ax3.set_aspect('equal')
ax4.set_aspect('equal')

fig.subplots_adjust(wspace=0,left=0.22,bottom=0.1,right=0.77,top=0.9)
fig2.subplots_adjust(wspace=0,left=0.22,bottom=0.1,right=0.77,top=0.9)

fig.savefig('heatmap_z2_help.eps', bbox_inches='tight')
fig2.savefig('heatmap_z2_payoff.eps', bbox_inches='tight')