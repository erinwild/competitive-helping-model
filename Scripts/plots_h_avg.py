import glob
import math
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import MultipleLocator

def extract_last_line(txt_file):
	last_lines = []
	for filename in glob.iglob(txt_file):
		file_data = open(filename, 'r')
		lines = file_data.readlines()
		file_data.close()
		last = lines[-1].split()
		for i in range(0,len(last)):
			last[i] = float(last[i])
		last_lines.append(last)
	last_lines = np.array(last_lines)
	return last_lines


path_prefix = '/Volumes/Optical Bay HD/Dropbox/School/Doctoral Research/Competitive Helping Model/Simulations'

## k plots ##
k_labels = ['0', '0.5', '1', '1.5', '2']
k_data = []
k_h_avg = []
for i in range(0,len(k_labels)):
	path_name = (path_prefix + '/Varying k/*k%s-*.txt') % k_labels[i]
	k_data.append(extract_last_line(path_name))
	k_h_avg.append(k_data[i][:,4])

fig = plt.figure()
ax = fig.add_subplot(111)
bp = ax.boxplot(k_h_avg, notch=1)
plt.title('Average help produced for different k values')
plt.ylabel('h')
plt.xlabel('k')
ax.yaxis.set_major_locator(MultipleLocator(1))
ax.yaxis.set_minor_locator(MultipleLocator(0.5))
ax.set_ylim(0, 3)
plt.setp(bp['whiskers'], color='black')
plt.setp(bp['fliers'], color='red', marker='+')
ax.set_xticklabels(k_labels)


## m plots ##
m_labels = ['1', '5', '10', '20']
m_data = []
m_h_avg = []
for i in range(0,len(m_labels)):
	path_name = (path_prefix + '/Varying m/*m%sx*.txt') % m_labels[i]
	m_data.append(extract_last_line(path_name))
	m_h_avg.append(m_data[i][:,4])

fig = plt.figure()
ax = fig.add_subplot(111)
bp = ax.boxplot(m_h_avg, notch=1)
plt.title('Average help produced for different m values')
plt.ylabel('h')
plt.xlabel('m')
ax.yaxis.set_major_locator(MultipleLocator(1))
ax.yaxis.set_minor_locator(MultipleLocator(0.5))
plt.setp(bp['whiskers'], color='black')
plt.setp(bp['fliers'], color='red', marker='+')
ax.set_xticklabels(m_labels)


## x plots ##
x_labels = ['0.1', '0.2', '0.3', '0.4', '0.5', '0.6', '0.7', '0.8', '0.9']
x_pos = [1, 2, 3, 4, 5, 6, 7, 8, 9]
h_two_groups = []
h_theoret = []
x_data = []
x_h_avg = []
for i in range(0,len(x_labels)):
	path_name = (path_prefix + '/Varying x/*x%sz*.txt') % x_labels[i]
	x_data.append(extract_last_line(path_name))
	x_h_avg.append(x_data[i][:,4])
	path_name = (path_prefix + '/h_m vs h_p - S_m equals S_p/*x-%s-*Run1.txt') % x_labels[i] ##
	run = extract_last_line(path_name) ##
	h_two_groups.append(run[:,0]) ##
	h_two_groups[i] = h_two_groups[i]*10 ##
	h_theoret.append(10*(-x_pos[i]/10.0+math.sqrt(x_pos[i]/10.0))) ##

fig = plt.figure()
ax = fig.add_subplot(111)
bp = ax.boxplot(x_h_avg, notch=1)
ax.plot(x_pos, h_two_groups) ##
ax.plot(x_pos, h_theoret) ##
plt.title('Average help produced for different x values')
plt.ylabel('h')
plt.xlabel('x')
ax.yaxis.set_major_locator(MultipleLocator(1))
ax.yaxis.set_minor_locator(MultipleLocator(0.5))
plt.setp(bp['whiskers'], color='black')
plt.setp(bp['fliers'], color='red', marker='+')
ax.set_xticklabels(x_labels)


## z plots ##
z_labels = ['0', '0.25', '0.5', '0.75', '1', '1.25', '1.5', '1.75', '2', '2.25', '2.5', '2.75', '3']
z_data = []
z_h_avg = []
for i in range(0,len(z_labels)):
	path_name = (path_prefix + '/Varying z/*z%sk*.txt') % z_labels[i]
	z_data.append(extract_last_line(path_name))
	z_h_avg.append(z_data[i][:,4])

fig = plt.figure()
ax = fig.add_subplot(111)
bp = ax.boxplot(z_h_avg, notch=1)
plt.title('Average help produced for different z values')
plt.ylabel('h')
plt.xlabel('z')
ax.yaxis.set_major_locator(MultipleLocator(1))
ax.yaxis.set_minor_locator(MultipleLocator(0.5))
plt.setp(bp['whiskers'], color='black')
plt.setp(bp['fliers'], color='red', marker='+')
ax.set_xticklabels(z_labels)


plt.show()