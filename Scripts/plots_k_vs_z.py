import glob
import numpy as np
from collections import Counter
import matplotlib.pyplot as plt

def h_star(m, x, z, k, N):
	return (-x+np.sqrt(x*(1/2.0)*(N-2)*((z+1)*N-1))/float(N-1))*m-k

m = 1
x = 0.5
N = 100000
num_runs = 5000

path_prefix = '/Volumes/Optical Bay HD/Dropbox/School/Doctoral Research/Competitive Helping Model/Simulations/k vs z'

light_blue = '#06AED5'
dark_blue = '#086788'
yellow = '#F0C808'
bone = '#FFF1D0'
red = '#DD1C1A'
colours = [dark_blue,light_blue,red]

c_index = 0
for z in [2, 5, 10]:
	count = []
	for k in np.arange(0, 0.70001, 0.005):
	
		path_name = path_prefix + '/z%r-k%r.txt' % (z, round(k, 3))

		for filename in glob.iglob(path_name):
			file_data = open(filename, 'r')
			lines = file_data.readlines()
			file_data.close()
			lines = np.loadtxt(lines)
			lines = np.round(lines[:,0], 3)

		h = round(h_star(m, x, z, k, N), 3)
		if h > 1:
			h = 1
		elif h < 0:
			h = 0
		count.append((lines == h).sum())
	z_label = 'z = %r' % z 
	plt.plot(np.arange(0, 0.70001, 0.005), count, linewidth=1.5, color=colours[c_index], label=z_label)
	c_index += 1

plt.xlabel('k')
plt.ylabel('frequency of competitive helping (%)')
plt.ylim(0, num_runs)
plt.xlim(0, 0.7)
plt.yticks(np.arange(0, num_runs+1, num_runs/5),[0, 20, 40, 60, 80, 100])
plt.legend(loc=4)
plt.show()
