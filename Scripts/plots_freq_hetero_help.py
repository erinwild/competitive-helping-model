import glob
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl

path_prefix = '/Volumes/Optical Bay HD/Dropbox/School/Doctoral Research/Competitive Helping Model/Simulations/Heterogeneous groups'

fig, axes = plt.subplots(figsize=(6,6), nrows=8, sharex=True, sharey=True)

paths = ['/z-0-*.txt', '/z-0.5-*.txt', '/z-1-*.txt', '/z-1.5-*.txt', '/z-2-*.txt', '/z-5-*.txt', '/z-10-*.txt', '/z-100-*.txt']

cm = plt.cm.get_cmap('plasma_r')
time = 4999
payoffs = [0, 0.085781951, 0.05741409, 0.0015898, -0.333333333333333]
scaled_payoffs = [(payoffs[i]-(-0.4))/(0.1-(-0.4)) for i in range(5)]
index = 0
z_label = ['z = 0', 'z = 0.5', 'z = 1', 'z = 1.5', 'z = 2', 'z = 5', 'z = 10', 'z = 100']
for path_match in paths:
	for filename in glob.iglob(path_prefix + path_match):
		lines = np.loadtxt(filename)
		lines = lines[:,1:402]
	help = lines[time,0:200]
	num, bins, bars = axes[index].hist(help, bins=[0.05*i for i in range(0,21)])
	plt.setp(bars[0], 'facecolor', cm(scaled_payoffs[0]))
	plt.setp(bars[4], 'facecolor', cm(scaled_payoffs[1]))
	plt.setp(bars[7], 'facecolor', cm(scaled_payoffs[2]))
	plt.setp(bars[9], 'facecolor', cm(scaled_payoffs[3]))
	plt.setp(bars[-1], 'facecolor', cm(scaled_payoffs[4]))
	axes[index].set_yticklabels([0, '', '', '', 200])
	axes[index].set_xticks([0.1*i for i in range(0,11)])
	axes[index].set_xticklabels([0, '', 0.2, '', 0.4, '', 0.6, '', 0.8, '', 1])
	tl = axes[index].get_yticklabels()
	tl[0].set_verticalalignment('bottom')
	tl[-1].set_verticalalignment('top')
	#if (index % 2) != 0:
	#	axes[index].yaxis.tick_right()
	#	axes[index].yaxis.set_ticks_position('both')
	axes[index].text(0.97, 0.93, z_label[index],
		horizontalalignment='right',
		verticalalignment='top',
		fontsize='medium',
		transform=axes[index].transAxes)
	index += 1

tl = axes[-1].get_xticklabels()
tl[0].set_horizontalalignment('left')
tl[-1].set_horizontalalignment('right')
axes[-1].set_xlabel('h')

cax = fig.add_axes([0.68, 0.1095, 0.03, 0.78])
norm = mpl.colors.Normalize(vmin=-0.4, vmax=0.1)
cbar = mpl.colorbar.ColorbarBase(cax, ticks=[-0.333,0.00,0.057,0.086], cmap=cm, norm=norm)
fig.subplots_adjust(right=0.6)
fig.text(0.03, 0.5, 'frequency', va='center', rotation='vertical')
fig.text(0.75, 0.5, 'payoff', va='center', rotation='vertical')

fig.savefig('freq_hetero_help.eps', bbox_inches='tight')
#plt.show()