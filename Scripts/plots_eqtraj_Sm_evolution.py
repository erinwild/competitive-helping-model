import glob
import numpy as np
import matplotlib.pyplot as plt

path_prefix = '/Users/Erin/Dropbox/School/Doctoral Research/Competitive Helping Model/Simulations/Sm evolution'

fig, ax = plt.subplots(ncols=1, sharex=True, sharey=True)

for i in range(1,5):
	path_name = (path_prefix + '/z-2-*Run%s*.txt') % str(i)
	for filename in glob.iglob(path_name):
		lines = np.loadtxt(filename, usecols=(1, 2), unpack=True)
		
		ax.plot(lines[0], lines[1], c='0.45')
		ax.plot(lines[0][-1], lines[1][-1], 'kD')

ax.set_ylabel('h$_m$')
ax.set_xlabel('h$_p$')
ax.set(adjustable='box-forced', aspect='equal')
plt.axis([-0.02, 1.02, -0.02, 1.02])

plt.show()