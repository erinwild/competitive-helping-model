import java.io.*;

/** 
 * @author Erin Wild eewild@gmail.com
 * @url https://bitbucket.org/erinwild/competitive-helping-model/
 **/

public class PopulationGroups extends Population {
	// PopulationGroups can be used in place of Population when there are different groups comprising homogeneous Individuals
	private int numGroups;
	public Group[] groups;
	private double totalHelpAvailable;
	
	PopulationGroups(int populationSize, int numGroups) {
		groups = new Group[numGroups];
		for(int i = 0; i < numGroups; i++) {
			groups[i] = new Group();
		}
		setPopulationSize(populationSize);
		this.numGroups = numGroups;
	}
	
	public int getNumGroups() {
		return numGroups;
	}
	
	public void initializeAttributes(int[] groupPopSizes, double[] groupHelp, double[] m, double[] x, double[] z, double[] k) {
		int sum = 0;
		for(int i = 0; i < numGroups; i++) {
			sum += groupPopSizes[i];
			groups[i].setGroupSize(groupPopSizes[i]);
			groups[i].setHelp(groupHelp[i]);
			groups[i].setMaxFitnessBenefits(m[i]);
			groups[i].setBenefitsCurve(x[i]);
			groups[i].setPartnerChoice(z[i]);
			groups[i].setPassiveHelp(k[i]);
		}
		recalculateHelpAvailable();
		if(sum != this.getPopulationSize()) {
			throw new IllegalArgumentException("The sum of the sizes of all groups must be the same as the overall population size.");
		}
	}
	
	public void recalculateHelpAvailable() {
		totalHelpAvailable = 0;
		for(int i = 0; i < numGroups; i++) {
			totalHelpAvailable += Math.pow(groups[i].getHelp() + groups[i].getPassiveHelp(),groups[i].getPartnerChoice()) * groups[i].getGroupSize();
		}
	}
	
	public double getHelpAvailable() {
		return totalHelpAvailable;
	}

	public void calculateHelpReceived() {
		double sum;
		double iHelp;
		double jHelp;
		recalculateHelpAvailable();
		for(int i = 0; i < numGroups; i++) {
			sum = 0;
			iHelp = Math.pow(groups[i].getHelp() + groups[i].getPassiveHelp(),groups[i].getPartnerChoice());
			if(groups[i].getGroupSize() > 1 && totalHelpAvailable != iHelp) {
				sum = (groups[i].getGroupSize() - 1) * (groups[i].getHelp() + groups[i].getPassiveHelp()) * (iHelp / (totalHelpAvailable - iHelp));
			}
			for(int j = 0; j < numGroups; j++) {
				if(j != i) {
					jHelp = Math.pow(groups[j].getHelp() + groups[j].getPassiveHelp(),groups[j].getPartnerChoice());
					if(totalHelpAvailable != jHelp) {
						sum += groups[j].getGroupSize() * (groups[j].getHelp() + groups[j].getPassiveHelp()) * (iHelp / (totalHelpAvailable - jHelp));
					}
				}
			}
			groups[i].setHelpReceived(sum);
		}
	}
	
	public void calculatePayoffs() {
		for(int i = 0; i < numGroups; i++) {
			groups[i].calculatePayoff();
		}
		
	}
	
	public class Group extends Individual {
		private long groupSize;
		
		public void setGroupSize(long groupSize) {
			if(groupSize < 0 || groupSize > getPopulationSize()) {
				throw new IllegalArgumentException("The size of a group must not be less than zero or greater than the total number of Individuals in the Population.");
			} else {
				this.groupSize = groupSize;
			}
		}
		
		public long getGroupSize() {
			return groupSize;
		}
	}
	
}