import java.io.*;
import java.util.*;

/** 
 * @author Erin Wild eewild@gmail.com
 * @url https://bitbucket.org/erinwild/competitive-helping-model/
 **/

public class Population {
	private int populationSize;
	public Individual[] individuals;
	private double totalHelpAvailable; // sum of help provided by all Individuals in the Population
	
	public Population() {
	}
	
	public Population(int populationSize) {
		individuals = new Individual[populationSize];
		for(int i = 0; i < populationSize; i++) {
			individuals[i] = new Individual();
		}
		setPopulationSize(populationSize);
	}
	
	public void setPopulationSize(int populationSize) {
		this.populationSize = populationSize;
	}
	
	public int getPopulationSize() {
		return populationSize;
	}
	
	public void initializeAttributes(double[] help, double[] m, double[] x, double[] z, double[] k) {
		// assigns each Individual their starting strategy and attributes
		for(int i = 0; i < getPopulationSize(); i++) {
			individuals[i].setHelp(help[i]);
			individuals[i].setMaxFitnessBenefits(m[i]);
			individuals[i].setBenefitsCurve(x[i]);
			individuals[i].setPartnerChoice(z[i]);
			individuals[i].setPassiveHelp(k[i]);
		}
		recalculateHelpAvailable();
	}
	
	public void initializeAttributes(double help, double m, double x, double z, double k) {
		// assigns each Individual the same starting strategy and attributes
		for(int i = 0; i < getPopulationSize(); i++) {
			individuals[i].setHelp(help);
			individuals[i].setMaxFitnessBenefits(m);
			individuals[i].setBenefitsCurve(x);
			individuals[i].setPartnerChoice(z);
			individuals[i].setPassiveHelp(k);
		}
		recalculateHelpAvailable();
	}
	
	public void recalculateHelpAvailable() {
		// recalculates total help available in the population
		totalHelpAvailable = 0;
		for(int i = 0; i < populationSize; i++) {
			totalHelpAvailable += Math.pow(individuals[i].getHelp() + individuals[i].getPassiveHelp(),individuals[i].getPartnerChoice());
		}
	}
	
	public double getHelpAvailable() {
		return totalHelpAvailable;
	}
	
	public void calculateHelpReceived() {
		// calculates sum of help received for each Individual from each other Individual in the Population
		double sum;
		double iHelp;
		double jHelp;
		recalculateHelpAvailable();
		for(int i = 0; i < populationSize; i++) {
			sum = 0;
			iHelp = Math.pow(individuals[i].getHelp() + individuals[i].getPassiveHelp(),individuals[i].getPartnerChoice());
			for(int j = 0; j < populationSize; j++) {
				if(j != i) {
					jHelp = Math.pow(individuals[j].getHelp() + individuals[j].getPassiveHelp(),individuals[j].getPartnerChoice());
					if(totalHelpAvailable != jHelp) {
						sum += (individuals[j].getHelp() + individuals[j].getPassiveHelp()) * (iHelp / (totalHelpAvailable - jHelp));
					}
				}
			}
			individuals[i].setHelpReceived(sum);
		}
	}
	
	public void calculatePayoffs() {
		// each Individual calculates their respective payoff
		for(int i = 0; i < populationSize; i++) {
			individuals[i].calculatePayoff();
		}
	}
	
	public static void clone(Individual clone, Individual discard) {
		// transform discard into clone by copying all values from clone
		discard.setHelp(clone.getHelp());
		discard.setMaxFitnessBenefits(clone.getMaxFitnessBenefits());
		discard.setBenefitsCurve(clone.getBenefitsCurve());
		discard.setPartnerChoice(clone.getPartnerChoice());
		discard.setPassiveHelp(clone.getPassiveHelp());
	}
	
	public static void crossover(Individual parentOne, Individual parentTwo) {
		// uses uniform crossover to transform parents into new offspring
		MersenneTwisterFast rng = new MersenneTwisterFast();
		double swap;
		
		// swap help
		if(rng.nextDouble() >= 0.5) {
			swap = parentOne.getHelp();
			parentOne.setHelp(parentTwo.getHelp());
			parentTwo.setHelp(swap);
		}
		
		// swap max fitness benefits
		if(rng.nextDouble() >= 0.5) {
			swap = parentOne.getMaxFitnessBenefits();
			parentOne.setMaxFitnessBenefits(parentTwo.getMaxFitnessBenefits());
			parentTwo.setMaxFitnessBenefits(swap);
		}
		
		// swap benefits curve
		if(rng.nextDouble() >= 0.5) {
			swap = parentOne.getBenefitsCurve();
			parentOne.setBenefitsCurve(parentTwo.getBenefitsCurve());
			parentTwo.setBenefitsCurve(swap);
		}
		
		// swap partner choice
		if(rng.nextDouble() >= 0.5) {
			swap = parentOne.getPartnerChoice();
			parentOne.setPartnerChoice(parentTwo.getPartnerChoice());
			parentTwo.setPartnerChoice(swap);
		}
		
		// swap passive help
		if(rng.nextDouble() >= 0.5) {
			swap = parentOne.getPassiveHelp();
			parentOne.setPassiveHelp(parentTwo.getPassiveHelp());
			parentTwo.setPassiveHelp(swap);
		}
	}
	
	public static void mutation(Individual mutant, double rate, double maxHelp) {
		// mutates help of an Individual based on the mutation rate
		MersenneTwisterFast rng = new MersenneTwisterFast();
		if(rng.nextDouble() <= rate) {
			mutant.setHelp(rng.nextDouble() * maxHelp);
		}
	}
	
	public static void mutation(Individual mutant, double rate, double maxHelp, double maxM, double maxZ, double maxK) {
		// mutates all parameters of an Individual based on the mutation rate
		MersenneTwisterFast rng = new MersenneTwisterFast();
		if(rng.nextDouble() <= rate) {
			mutant.setHelp(rng.nextDouble()*maxHelp);
		}
		if(rng.nextDouble() <= rate) {
			mutant.setMaxFitnessBenefits(rng.nextDouble()*maxM);
		}
		if(rng.nextDouble() <= rate) {
			mutant.setBenefitsCurve(rng.nextDouble());
		}
		if(rng.nextDouble() <= rate) {
			mutant.setPartnerChoice(rng.nextDouble()*maxZ);
		}
		if(rng.nextDouble() <= rate) {
			mutant.setPassiveHelp(rng.nextDouble()*maxK);
		}
	}
	
	/* Individuals exist as part of a Population */
	public class Individual {
		private double mMaxFitnessBenefits;
		private double xBenefitsCurve;
		private double zPartnerChoice;
		private double kPassiveHelp;
		private double hHelpProvided;
		private double rHelpReceived;
		private double wPayoff;
		
		public Individual() {
		}
		
		public Individual(double h) {
			setHelp(h);
		}
		
		public void setHelp(double h) {
			// sets the level of active help provided by the Individual and updates total help available in the Population
			if(h < 0) {
				throw new IllegalArgumentException("Level of active help must be a positive number.");
			} else {
				hHelpProvided = h;
			}
		}
		
		public double getHelp() {
			return hHelpProvided;
		}
		
		public void setMaxFitnessBenefits(double m) {
			// sets the degree of partner choice for the Individual
			if(m <= 0) {
				throw new IllegalArgumentException("Maximum fitness benefits must be a positive number.");
			} else {
				mMaxFitnessBenefits = m;
			}
		}
		
		public double getMaxFitnessBenefits() {
			return mMaxFitnessBenefits;
		}
		
		public void setBenefitsCurve(double x) {
			// sets the benefits curve for the Individual
			if(x < 0 || x > 1) {
				throw new IllegalArgumentException("Benefits curve must be between 0 and 1.");
			} else {
				xBenefitsCurve = x;
			}
		}
		
		public double getBenefitsCurve() {
			return xBenefitsCurve;
		}
		
		public void setPartnerChoice(double z) {
			// sets the degree of partner choice for the Individual
			if(z < 0) {
				throw new IllegalArgumentException("Degree of partner choice must be a positive number.");
			} else {
				zPartnerChoice = z;
			}
		}
		
		public double getPartnerChoice() {
			return zPartnerChoice;
		}
		
		public void setPassiveHelp(double k) {
			// sets the amount of passive help provided by the Individual
			if(k < 0) {
				throw new IllegalArgumentException("Passive help must be a positive number.");
			} else {
				kPassiveHelp = k;
			}
		}
		
		public double getPassiveHelp() {
			return kPassiveHelp;
		}
		
		public void setHelpReceived(double r) {
			// sets the help received by the Individual
			if(r < 0) {
				throw new IllegalArgumentException("Help received must be a positive number.");
			} else {
				rHelpReceived = r;
			}
		}
		
		public double getHelpReceived() {
			return rHelpReceived;
		}
		
		public void calculatePayoff() {
			// calculates the Individual's payoff
			double r = getHelpReceived();
			double m = getMaxFitnessBenefits();
			double x = getBenefitsCurve();
			double h = getHelp();
			wPayoff = ((double)(m*r))/(m*x+r)-h;
		}
		
		public void setPayoff(double w) {
			wPayoff = w;
		}
		
		public double getPayoff() {
			// returns the Individual's current payoff, without recalculating it
			return wPayoff;
		}
	}
}